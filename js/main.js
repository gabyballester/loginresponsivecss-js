//Creamos constante con los selectores .input
const inputs = document.querySelectorAll(".input");

function addcl(){
	let parent = this.parentNode.parentNode; //crea variable con el nodo padre
	parent.classList.add("focus"); //añade la clase focus al parent
}

function remcl(){
	let parent = this.parentNode.parentNode;
	if(this.value == ""){
		parent.classList.remove("focus");
	}
}

inputs.forEach(input => { //recorre los inputs
	input.addEventListener("focus", addcl); //añadimos listener para focus y ejecuta addcl1
	input.addEventListener("blur", remcl); //añadimos listener para blur y ejecuta remcl
});